<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\ShorterPathReferrerStrategy;
use PHPUnit\Framework\TestCase;

/**
 * ShorterPathReferrerStrategyTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\ShorterPathReferrerStrategy
 *
 * @internal
 *
 * @small
 */
class ShorterPathReferrerStrategyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ShorterPathReferrerStrategy
	 */
	protected ShorterPathReferrerStrategy $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ShorterPathReferrerStrategy();
	}
	
}
