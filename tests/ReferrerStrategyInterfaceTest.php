<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\ReferrerStrategyInterface;
use PHPUnit\Framework\TestCase;

/**
 * ReferrerStrategyInterfaceTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\ReferrerStrategyInterface
 *
 * @internal
 *
 * @small
 */
class ReferrerStrategyInterfaceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ReferrerStrategyInterface
	 */
	protected ReferrerStrategyInterface $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(ReferrerStrategyInterface::class);
	}
	
}
