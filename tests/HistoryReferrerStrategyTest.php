<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\HistoryReferrerStrategy;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UriFactoryInterface;

/**
 * HistoryReferrerStrategyTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\HistoryReferrerStrategy
 *
 * @internal
 *
 * @small
 */
class HistoryReferrerStrategyTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var HistoryReferrerStrategy
	 */
	protected HistoryReferrerStrategy $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new HistoryReferrerStrategy(
			$this->getMockForAbstractClass(UriFactoryInterface::class),
		);
	}
	
}
