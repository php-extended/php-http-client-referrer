<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;

/**
 * ShorterPathReferrerStrategy class file.
 * 
 * This class represents a strategy that adds most of the path from the host
 * as referrer.
 * 
 * @author Anastaszor
 */
class ShorterPathReferrerStrategy implements ReferrerStrategyInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpClient\ReferrerStrategyInterface::applyOnRequest()
	 */
	public function applyOnRequest(RequestInterface $request) : RequestInterface
	{
		$uri = $request->getUri();
		$path = \str_replace('\\', '/', \dirname($uri->getPath())); // sometimes dirname does strange things on windows
		
		try
		{
			return $request
				->withoutHeader('Referer') // remove all before add
				->withHeader('Referer', $uri->getScheme().'://'.\rtrim($uri->getHost(), '/').'/'.\ltrim($path, '/'))
			;
		}
		catch(InvalidArgumentException $e)
		{
			return $request;
		}
	}
	
}
