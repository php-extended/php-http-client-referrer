<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Message\RequestInterface;

/**
 * NoOpReferrerStrategy class file.
 * 
 * This strategy does nothing to the request.
 * 
 * @author Anastaszor
 */
class NoOpReferrerStrategy implements ReferrerStrategyInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpClient\ReferrerStrategyInterface::applyOnRequest()
	 */
	public function applyOnRequest(RequestInterface $request) : RequestInterface
	{
		return $request;
	}
	
}
