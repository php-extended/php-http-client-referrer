<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;

/**
 * HostReferrerStrategy class file.
 * 
 * This class represents a strategy that adds the host as referrer.
 * 
 * @author Anastaszor
 */
class HostReferrerStrategy implements ReferrerStrategyInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpClient\ReferrerStrategyInterface::applyOnRequest()
	 */
	public function applyOnRequest(RequestInterface $request) : RequestInterface
	{
		try
		{
			return $request->withHeader('Referer', $request->getUri()->getScheme().'://'.$request->getUri()->getHost());
		}
		catch(InvalidArgumentException $e)
		{
			return $request;
		}
	}
	
}
