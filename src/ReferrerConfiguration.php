<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Stringable;

/**
 * ReferrerConfiguration class file.
 * 
 * This class represents the configuration for the client.
 * 
 * @author Anastaszor
 */
class ReferrerConfiguration implements Stringable
{
	
	/**
	 * Gets whether to override existing referrer values.
	 * 
	 * @var boolean
	 */
	protected bool $_override = true;
	
	/**
	 * The strategy to apply on the request.
	 * 
	 * @var ?ReferrerStrategyInterface
	 */
	protected ?ReferrerStrategyInterface $_strategy = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Enables whether to override the referer header.
	 */
	public function enableOverride() : void
	{
		$this->_override = true;
	}
	
	/**
	 * Disables whether to override the referer header.
	 */
	public function disableOverride() : void
	{
		$this->_override = false;
	}
	
	/**
	 * Gets whether the override of the header is allowed.
	 * 
	 * @return boolean
	 */
	public function hasOverride() : bool
	{
		return $this->_override;
	}
	
	/**
	 * Sets the strategy to use in the client.
	 * 
	 * @param ReferrerStrategyInterface $strategy
	 */
	public function setStrategy(ReferrerStrategyInterface $strategy) : void
	{
		$this->_strategy = $strategy;
	}
	
	/**
	 * Gets the strategy to use in the client.
	 * 
	 * @return ReferrerStrategyInterface
	 */
	public function getStrategy() : ReferrerStrategyInterface
	{
		if(null === $this->_strategy)
		{
			$this->_strategy = new ShorterPathReferrerStrategy();
		}
		
		return $this->_strategy;
	}
	
}
