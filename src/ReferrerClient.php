<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * ReferrerClient class file.
 * 
 * This class is an implementation of a client which adds host headers of
 * incoming requests.
 * 
 * @author Anastaszor
 */
class ReferrerClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The referrer configuration.
	 * 
	 * @var ReferrerConfiguration
	 */
	protected ReferrerConfiguration $_configuration;
	
	/**
	 * Builds a new ReferrerClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 * @param ?ReferrerConfiguration $configuration
	 */
	public function __construct(ClientInterface $client, ?ReferrerConfiguration $configuration = null)
	{
		$this->_client = $client;
		if(null === $configuration)
		{
			$configuration = new ReferrerConfiguration();
		}
		$this->_configuration = $configuration;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if(!$request->hasHeader('Referer') || $this->_configuration->hasOverride())
		{
			$request = $this->_configuration->getStrategy()->applyOnRequest($request);
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
