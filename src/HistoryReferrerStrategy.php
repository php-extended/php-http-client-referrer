<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;

/**
 * HistoryReferrerStrategy class file.
 * 
 * This class takes the last used uri as referrer.
 * 
 * @author Anastaszor
 */
class HistoryReferrerStrategy implements ReferrerStrategyInterface
{
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The previous uri.
	 * 
	 * @var ?UriInterface
	 */
	protected ?UriInterface $_previousUri = null;
	
	/**
	 * Builds a new HistoryReferrerStrategy with the given uri factory.
	 * 
	 * @param UriFactoryInterface $uriFactory
	 */
	public function __construct(UriFactoryInterface $uriFactory)
	{
		$this->_uriFactory = $uriFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\HttpClient\ReferrerStrategyInterface::applyOnRequest()
	 */
	public function applyOnRequest(RequestInterface $request) : RequestInterface
	{
		if(null !== $this->_previousUri)
		{
			try
			{
				$request = $request->withHeader('Referer', $this->_previousUri->__toString());
			}
			catch(InvalidArgumentException $e)
			{
				// just ignore
			}
		}
		
		$requestUri = $request->getUri();
		
		try
		{
			$this->_previousUri = $this->_uriFactory->createUri()
				->withScheme($requestUri->getScheme())
				->withHost($requestUri->getHost())
				->withPath($requestUri->getPath())
				->withQuery($requestUri->getQuery())
			;
		}
		catch(InvalidArgumentException $e)
		{
			// just ignore
		}
		
		return $request;
	}
	
}
