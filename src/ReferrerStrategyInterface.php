<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-referrer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Message\RequestInterface;
use Stringable;

/**
 * ReferrerStrategyInterface interface file.
 * 
 * This class represents a strategy to choose the referrer to put in the 
 * request.
 * 
 * @author Anastaszor
 */
interface ReferrerStrategyInterface extends Stringable
{
	
	/**
	 * Applies the referrer strategy on the given request, returns the modified
	 * request.
	 * 
	 * @param RequestInterface $request
	 * @return RequestInterface
	 */
	public function applyOnRequest(RequestInterface $request) : RequestInterface;
	
}
